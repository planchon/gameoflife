# Jeu de la vie - GameOfLife
Une implementation en Pascal du jeu de la vie par John Horton Conway. Travail réalisé en première année de prépa à l'EISTI. 

## Utilisation
* `gameOfLife_partieX -i ex/FICHIER_EXEMPLE` pour lancer une simulation
* `gameOfLife_partieX -o` pour enregistrer les simulations
* `gameOfLife_partieX -s` pour enregistrer une image des simulation
* `gameOfLife_partieX -l` pour logger les simulations


## Licence 
Copyright © Paul Planchon 2018
Vous pouvez utiliser / modifier ce logiciel si vous me citez dans votre logiciel. Les utilisations commerciales ne sont pas autorisées sans autorisation.